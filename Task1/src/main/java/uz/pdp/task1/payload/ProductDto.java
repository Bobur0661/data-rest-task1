package uz.pdp.task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {


    private Long id;
    private String name;
    private Long  categoryId;
    private Long  photoId;
    private Long  measurementId;


    public ProductDto(String name, Long categoryId, Long photoId, Long measurementId) {
        this.name = name;
        this.categoryId = categoryId;
        this.photoId = photoId;
        this.measurementId = measurementId;
    }
}
