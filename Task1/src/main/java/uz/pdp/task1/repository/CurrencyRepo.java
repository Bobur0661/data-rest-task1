package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task1.entity.Currency;
import uz.pdp.task1.projections.CustomCurrencyProjection;

@RepositoryRestResource(path = "currency", collectionResourceRel = "list", excerptProjection = CustomCurrencyProjection.class)
public interface CurrencyRepo extends JpaRepository<Currency, Long> {

//    boolean existsByName(String name);

}
