package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task1.entity.attachment.AttachmentContent;

public interface AttachmentContentRepo extends JpaRepository<AttachmentContent, Long> {
}
