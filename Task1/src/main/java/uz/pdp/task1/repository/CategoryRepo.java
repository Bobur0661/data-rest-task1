package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task1.entity.Category;

public interface CategoryRepo extends JpaRepository<Category, Long> {


}
