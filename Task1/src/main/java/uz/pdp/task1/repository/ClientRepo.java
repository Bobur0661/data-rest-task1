package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task1.entity.Client;
import uz.pdp.task1.projections.CustomClientProjection;
// path ==> /api/client yo'liga kelsin degani
@RepositoryRestResource(path = "client", collectionResourceRel = "list", excerptProjection = CustomClientProjection.class)
public interface ClientRepo extends JpaRepository<Client, Long> {

//    boolean existsByPhoneNumber(String phoneNumber);

}
