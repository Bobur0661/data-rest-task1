package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task1.entity.Warehouse;
import uz.pdp.task1.projections.CustomWarehouseProjection;

@RepositoryRestResource(path = "warehouse", collectionResourceRel = "list", excerptProjection = CustomWarehouseProjection.class)
public interface WarehouseRepo extends JpaRepository<Warehouse, Long> {

}
