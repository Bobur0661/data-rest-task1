package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task1.entity.Supplier;
import uz.pdp.task1.projections.CustomSupplierProjection;

@RepositoryRestResource(path = "supplier", collectionResourceRel = "list", excerptProjection = CustomSupplierProjection.class)
public interface SupplierRepo extends JpaRepository<Supplier, Long> {

//    boolean existsByPhoneNumber(String phoneNumber);

}
