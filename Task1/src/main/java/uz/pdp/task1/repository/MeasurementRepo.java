package uz.pdp.task1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task1.entity.Measurement;
import uz.pdp.task1.projections.CustomMeasurementProjection;

@RepositoryRestResource(path = "measurement", collectionResourceRel = "list", excerptProjection = CustomMeasurementProjection.class)
public interface MeasurementRepo extends JpaRepository<Measurement, Long> {


    boolean existsByName(String name);

}
