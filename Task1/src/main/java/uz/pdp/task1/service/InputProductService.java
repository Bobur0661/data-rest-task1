package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Input;
import uz.pdp.task1.entity.InputProduct;
import uz.pdp.task1.entity.Product;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.InputProductDto;
import uz.pdp.task1.repository.InputProductRepo;
import uz.pdp.task1.repository.InputRepo;
import uz.pdp.task1.repository.ProductRepo;

import java.util.Optional;

@Service
public class InputProductService {

    @Autowired
    InputProductRepo inputProductRepo;

    @Autowired
    ProductRepo productRepo;

    @Autowired
    InputRepo inputRepo;


    public ApiResponse addOrUpdate(InputProductDto dto) {
        InputProduct inputProduct = new InputProduct();
        if (dto.getId() != null) {
            inputProduct = inputProductRepo.getById(dto.getId());
        }
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (!optionalProduct.isPresent()) {
            return new ApiResponse(false, "Product Not Found!");
        }
        Optional<Input> optionalInput = inputRepo.findById(dto.getInputId());
        if (!optionalInput.isPresent()) {
            return new ApiResponse(false, "Input noty Found!");
        }

        inputProduct.setInput(optionalInput.get());
        inputProduct.setProduct(optionalProduct.get());
        inputProduct.setAmount(dto.getAmount());
        inputProduct.setPrice(dto.getPrice());
        inputProduct.setExpireDate(dto.getExpireDate());
        inputProductRepo.save(inputProduct);
        return new ApiResponse(true, dto.getId() != null ? "Edited" : "Saved");
    }


    public ApiResponse delete(Long id) {
        try {
            inputProductRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
