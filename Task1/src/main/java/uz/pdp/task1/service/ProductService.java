package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Category;
import uz.pdp.task1.entity.Measurement;
import uz.pdp.task1.entity.Product;
import uz.pdp.task1.entity.attachment.Attachment;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.ProductDto;
import uz.pdp.task1.repository.AttachmentRepo;
import uz.pdp.task1.repository.CategoryRepo;
import uz.pdp.task1.repository.MeasurementRepo;
import uz.pdp.task1.repository.ProductRepo;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepo productRepo;

    @Autowired
    CategoryRepo categoryRepo;

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    MeasurementRepo measurementRepo;

    public ApiResponse getById(Long id) {
        Optional<Product> optionalProduct = productRepo.findById(id);
        if (optionalProduct.isPresent()) {
            return new ApiResponse(true, "Success", optionalProduct.get());
        }
        return new ApiResponse(false, "Product not Found");
    }


    public ApiResponse addOrUpdate(ProductDto dto) {
        // Agar shunaqa maxsulot bo'lmasa yangi product yaratib olamiz:
        Product product = new Product();
        if (dto.getId() != null) {
            product = productRepo.getById(dto.getId());
        } else if (productRepo.existsByNameAndCategoryId(dto.getName(), dto.getCategoryId())) {
            return new ApiResponse(false, "This kind of product already exists in this category!");
        }

        Optional<Category> optionalCategory = categoryRepo.findById(dto.getCategoryId());
        if (!optionalCategory.isPresent()) {
            return new ApiResponse(false, "Cannot find Category!");
        }
        Optional<Attachment> optionalAttachment = attachmentRepo.findById(dto.getPhotoId());
        if (!optionalAttachment.isPresent()) {
            return new ApiResponse(false, "Cannot find Photo");
        }
        Optional<Measurement> optionalMeasurement = measurementRepo.findById(dto.getMeasurementId());
        if (!optionalMeasurement.isPresent()) {
            return new ApiResponse(false, "Cannot find Measurement");
        }

        product.setName(dto.getName());
        product.setCategory(optionalCategory.get());
        product.setPhoto(optionalAttachment.get());
        product.setMeasurement(optionalMeasurement.get());
        productRepo.save(product);
        return new ApiResponse(true, dto.getId() != null ? "Edited" : "Saved");
    }


    public ApiResponse delete(Long id) {
        Optional<Product> optionalProduct = productRepo.findById(id);
        if (!optionalProduct.isPresent()) {
            return new ApiResponse(false, "Error on Deleting!");
        }
        productRepo.delete(optionalProduct.get());
        return new ApiResponse(true, "Deleted!");

    }


}
