package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Client;
import uz.pdp.task1.entity.Currency;
import uz.pdp.task1.entity.Output;
import uz.pdp.task1.entity.Warehouse;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.OutputDto;
import uz.pdp.task1.repository.ClientRepo;
import uz.pdp.task1.repository.CurrencyRepo;
import uz.pdp.task1.repository.OutputRepo;
import uz.pdp.task1.repository.WarehouseRepo;

import java.util.Optional;

@Service
public class OutputService {

    @Autowired
    OutputRepo outputRepo;

    @Autowired
    WarehouseRepo warehouseRepo;

    @Autowired
    ClientRepo clientRepo;

    @Autowired
    CurrencyRepo currencyRepo;

    public ApiResponse getById(Long id) {
        Optional<Output> optionalOutput = outputRepo.findById(id);
        if (optionalOutput.isPresent()) {
            return new ApiResponse(true, "Success", optionalOutput.get());
        }
        return new ApiResponse(false, "Output not Found");
    }


    public ApiResponse addOrUpdate(OutputDto dto) {
        Output output = new Output();
        if (dto.getId() != null) {
            output = outputRepo.getById(dto.getId());
        }
        Optional<Warehouse> optionalWarehouse = warehouseRepo.findById(dto.getWarehouseId());
        if (!optionalWarehouse.isPresent()) {
            return new ApiResponse(false, "Cannot find Warehouse!");
        }
        Optional<Client> optionalClient = clientRepo.findById(dto.getClientId());
        if (!optionalClient.isPresent()) {
            return new ApiResponse(false, "Cannot find Client");
        }
        Optional<Currency> optionalCurrency = currencyRepo.findById(dto.getCurrencyId());
        if (!optionalCurrency.isPresent()) {
            return new ApiResponse(false, "Cannot find Currency");
        }

        output.setCode(dto.getCode());
        output.setCurrency(optionalCurrency.get());
        output.setClient(optionalClient.get());
        output.setWarehouse(optionalWarehouse.get());
        output.setFactureNumber(dto.getFactureNumber());
        outputRepo.save(output);
        return new ApiResponse(true, dto.getId() != null ? "Edited" : "Saved");
    }


    public ApiResponse delete(Long id) {
        Optional<Output> optionalOutput = outputRepo.findById(id);
        if (!optionalOutput.isPresent()) {
            return new ApiResponse(false, "Error on Deleting!");
        }
        outputRepo.delete(optionalOutput.get());
        return new ApiResponse(true, "Deleted!");

    }


}
