package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Output;
import uz.pdp.task1.entity.OutputProduct;
import uz.pdp.task1.entity.Product;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.OutputProductDto;
import uz.pdp.task1.repository.OutputProductRepo;
import uz.pdp.task1.repository.OutputRepo;
import uz.pdp.task1.repository.ProductRepo;

import java.util.Optional;

@Service
public class OutputProductService {

    @Autowired
    OutputProductRepo outputProductRepo;

    @Autowired
    ProductRepo productRepo;

    @Autowired
    OutputRepo outputRepo;


    public ApiResponse addOrUpdate(OutputProductDto dto) {
        OutputProduct outputProduct = new OutputProduct();
        if (dto.getId() != null) {
            outputProduct = outputProductRepo.getById(dto.getId());
        }
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (!optionalProduct.isPresent()) {
            return new ApiResponse(false, "Product Not Found!");
        }
        Optional<Output> optionalOutput = outputRepo.findById(dto.getOutputId());
        if (!optionalOutput.isPresent()) {
            return new ApiResponse(false, "Output not Found!");
        }

        outputProduct.setOutput(optionalOutput.get());
        outputProduct.setProduct(optionalProduct.get());
        outputProduct.setAmount(dto.getAmount());
        outputProduct.setPrice(dto.getPrice());
        outputProductRepo.save(outputProduct);
        return new ApiResponse(true, dto.getId() != null ? "Edited" : "Saved");
    }


    public ApiResponse delete(Long id) {
        try {
            outputProductRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
