package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Product;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.ProductDto;
import uz.pdp.task1.repository.ProductRepo;
import uz.pdp.task1.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {


    @Autowired
    ProductService productService;

    @Autowired
    ProductRepo productRepo;

    @GetMapping()
    public List<Product> get() {
        return productRepo.findAll();
    }

    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return productService.getById(id);
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addProduct(@RequestBody ProductDto dto) {
        return productService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return productService.delete(id);
    }


}
