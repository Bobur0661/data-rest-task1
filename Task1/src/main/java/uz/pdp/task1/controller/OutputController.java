package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Output;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.OutputDto;
import uz.pdp.task1.repository.OutputRepo;
import uz.pdp.task1.service.OutputService;

import java.util.List;

@RestController
@RequestMapping("/output")
public class OutputController {


    @Autowired
    OutputService outputService;

    @Autowired
    OutputRepo outputRepo;

    @GetMapping()
    public List<Output> get() {
        return outputRepo.findAll();
    }

    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return outputService.getById(id);
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody OutputDto dto) {
        return outputService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return outputService.delete(id);
    }


}
