package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Category;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CategoryDto;
import uz.pdp.task1.repository.CategoryRepo;
import uz.pdp.task1.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    CategoryRepo categoryRepo;


    @GetMapping
    public List<Category> getAllCategories() {
        return categoryRepo.findAll();
    }


    @PostMapping("/add")
    public ApiResponse addCategory(@RequestBody CategoryDto dto) {
        return categoryService.addCategory(dto);
    }


    @PutMapping("/edit/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody CategoryDto dto) {
        return categoryService.update(id, dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return categoryService.delete(id);
    }
}
