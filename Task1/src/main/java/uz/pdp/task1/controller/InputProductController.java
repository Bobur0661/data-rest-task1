package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.InputProduct;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.InputProductDto;
import uz.pdp.task1.repository.InputProductRepo;
import uz.pdp.task1.service.InputProductService;

import java.util.List;

@RestController
@RequestMapping("/inputProduct")
public class InputProductController {

    @Autowired
    InputProductService inputProductService;

    @Autowired
    InputProductRepo inputProductRepo;


    @GetMapping
    public List<InputProduct> get() {
        return inputProductRepo.findAll();
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody InputProductDto dto) {
        return inputProductService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return inputProductService.delete(id);
    }


}
