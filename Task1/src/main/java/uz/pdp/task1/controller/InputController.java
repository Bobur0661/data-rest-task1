package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Input;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.InputDto;
import uz.pdp.task1.repository.InputRepo;
import uz.pdp.task1.service.InputService;

import java.util.List;

@RestController
@RequestMapping("/input")
public class InputController {

    @Autowired
    InputService inputService;

    @Autowired
    InputRepo inputRepo;

    @GetMapping()
    public List<Input> get() {
        return inputRepo.findAll();
    }

    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return inputService.getById(id);
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody InputDto dto) {
        return inputService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return inputService.delete(id);
    }


}
