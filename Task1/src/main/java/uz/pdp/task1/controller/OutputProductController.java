package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.OutputProduct;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.OutputProductDto;
import uz.pdp.task1.repository.OutputProductRepo;
import uz.pdp.task1.service.OutputProductService;

import java.util.List;

@RestController
@RequestMapping("/outputProduct")
public class OutputProductController {

    @Autowired
    OutputProductService outputProductService;

    @Autowired
    OutputProductRepo outputProductRepo;


    @GetMapping
    public List<OutputProduct> get() {
        return outputProductRepo.findAll();
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody OutputProductDto dto) {
        return outputProductService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return outputProductService.delete(id);
    }


}
