package uz.pdp.task1.projections;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task1.entity.Currency;
import uz.pdp.task1.entity.Supplier;

@Projection(types = Currency.class)
public interface CustomCurrencyProjection {

    Integer getId();

    String getName();

    boolean getActive();

}
