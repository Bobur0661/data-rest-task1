package uz.pdp.task1.projections;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task1.entity.Currency;
import uz.pdp.task1.entity.Measurement;

@Projection(types = Measurement.class)
public interface CustomMeasurementProjection {

    Integer getId();

    String getName();

    boolean getActive();

}
