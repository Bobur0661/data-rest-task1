package uz.pdp.task1.projections;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task1.entity.Client;

@Projection(types = Client.class)
public interface CustomClientProjection {

    Integer getId();

    String getName();

    boolean getActive();

    String getPhoneNumber();

}
