package uz.pdp.task1.projections;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task1.entity.Supplier;

@Projection(types = Supplier.class)
public interface CustomSupplierProjection {

    Integer getId();

    String getName();

    boolean getActive();

    String getPhoneNumber();

}
