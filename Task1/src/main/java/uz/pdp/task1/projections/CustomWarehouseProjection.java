package uz.pdp.task1.projections;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task1.entity.Currency;
import uz.pdp.task1.entity.Warehouse;

@Projection(types = Warehouse.class)
public interface CustomWarehouseProjection {

    Integer getId();

    String getName();

    boolean getActive();

}
